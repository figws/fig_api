require("dotenv").config();
const db = require("../config/db.config");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

// User Register Service
exports.register = (data, callback) => {
  // Encrypt User's Passeord

  const saltRounds = 10;
  const password = data.password;

  bcrypt.hash(password, saltRounds, (err, hash) => {
    db.query(
      `INSERT INTO TblUsers (User_first_name, User_last_name, User_name, User_password, User_Type) VALUES (?, ?, ?, ?,?)`,
      [data.firstname, data.lastname, data.username, hash, "API_Role"],
      (error, results, fields) => {
        if (error) {
          return callback(error);
        }
        return callback(null, `User registered sucessfully`);
      }
    );
  });
};

// User Login Service
exports.login = (data, callback) => {
  db.query(
    `SELECT User_password from TblUsers where User_name= ? AND User_Type = 'API_Role'`,
    [data.username],
    (error, results, fields) => {
      if (error) {
        return callback(error);
      }

      if (!results.length) {
        return callback(new Error("Invalid Username"));
      } else {
        const db_password = results[0].User_password;

        bcrypt.compare(data.password, db_password, (err, res) => {
          if (err) return callback(err);
          if (!res) return callback(new Error("Invalid password"));

          try {
            const token = jwt.sign(
              {
                username: data.username,
                expire: Date.now() + 1000 * 60 * 60, //1 hour
              },
              process.env.JWT_SECRET
            );
            return callback(null, { token: token });
          } catch (error) {
            console.log("token sign error", error);
          }
        });
      }
    }
  );
};
