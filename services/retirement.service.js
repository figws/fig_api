const db = require("../config/db.config");

// Get Countires Service
exports.getCountries = (data, callback) => {
  db.query(
    `SELECT Country_id,Country_Name from TblCountry`,
    [],
    (error, results, fields) => {
      if (error) {
        return callback(error);
      }
      return callback(null, results);
    }
  );
};

// Get Future Exoenses
exports.getFutureExpenses = (data, callback) => {
  db.query(
    `SELECT inflation_rate from TblCountry where Country_id = ?`,
    [data.livein],
    (error, results, fields) => {
      if (error) {
        return callback(error);
      }
      if (results.length) {
        var inflation_rate = results[0].inflation_rate;
        var age = parseInt(data.age);
        var expenses = parseInt(data.expenses);
        var annual_expenses = expenses * 12;
        var cumulative_expenses2 = 0;
        var cumulative_expenses = annual_expenses;
        var futureexpenses = [];

        futureexpenses.push({
          age: age,
          cumulative: cumulative_expenses,
          annualexpenses: annual_expenses,
        });

        for (i = age; i < 85; i++) {
          cumulative_expenses2 = cumulative_expenses;

          age += 1;

          annual_expenses = Math.round(annual_expenses * (1 + inflation_rate));
          cumulative_expenses = cumulative_expenses + annual_expenses;
          futureexpenses.push({
            age: age,
            cumulative: cumulative_expenses,
            annualExpenses: annual_expenses,
          });
        }
        return callback(null, futureexpenses);
      } else {
        return callback(new Error("Bad Request"));
      }
    }
  );
};
