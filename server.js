require("dotenv").config();
const express = require("express");
const app = express();
const swaggerUi = require("swagger-ui-express");
const swaggerJsdoc = require("swagger-jsdoc");
const flatCache = require("flat-cache");
const cors = require("cors");

const bodyParser = require("body-parser");

app.use(cors());
app.use(bodyParser.json());

// use swagger-Ui-express for your app documentation endpoint
/** Swagger Initialization - START */

const options = {
  swaggerDefinition: {
    openapi: "3.0.1",
    info: {
      title: "FIG API Webservices",
      version: "1.0.0",
    },
    baseUrl: "https://api.first.com.jo",
    servers: [
      {
        url: "http://localhost:3000",
        description: "This is the Dev server (Localhost)",
      },
      {
        url: "http://stg-api.first.com.jo",
        description: "This is the STG server (Stagging)",
      },
      {
        url: "http://prod-api.first.com.jo",
        description: "This is the PROD server (Production)",
      },
    ],
    components: {
      securitySchemes: {
        bearerAuth: {
          type: "http",
          scheme: "bearer",
          bearerFormat: "bearer",
        },
      },
    },
  },
  apis: ["./routes/*.js"],
};
const swaggerSpecs = swaggerJsdoc(options);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerSpecs));

/** Swagger Initialization - END */

app.get("/", (req, res) => {
  res.send("Welcome to FIG API portal!");
});

// Routes to Users model
const userRoute = require("./routes/user.route");
app.use("/api/user", userRoute);

// Routes to Retirement model
const retirementRoute = require("./routes/retirement.route");
app.use("/api/retirement", retirementRoute);

// load new cache
let cache = flatCache.load("getCache");
// optionally, you can go ahead and pass the directory you want your
// cache to be loaded from by using this
// let cache = flatCache.load('productsCache', path.resolve('./path/to/folder')

// Initialize Helmet
var helmet = require("helmet");
app.use(helmet());

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
