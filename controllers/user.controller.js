const userService = require("../services/user.service");
const Joi = require("joi");

// User Register Controller
exports.register = (req, res, next) => {
  // Validation
  const schema = Joi.object({
    firstname: Joi.string().min(3).required(),
    lastname: Joi.string().min(3).required(),
    username: Joi.string().email().required(),
    password: Joi.string().min(8).required(),
  });

  const result = schema.validate(req.body);
  if (result.error) {
    res.status(400).send(result.error.details[0].message);
    return;
  }

  const data = {
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    username: req.body.username,
    password: req.body.password,
  };

  userService.register(data, (error, results) => {
    if (error) {
      return res.status(400).send(error.message);
    }
    return res.status(200).send({ message: results });
  });
};

// User Login Controller
exports.login = (req, res, next) => {
  // Validation
  const schema = Joi.object({
    username: Joi.string().email().required(),
    password: Joi.string().min(8).required(),
  });

  const result = schema.validate(req.body);
  if (result.error) {
    res.status(400).send(result.error.details[0].message);
    return;
  }

  const data = {
    username: req.body.username,
    password: req.body.password,
  };

  userService.login(data, (error, results) => {
    if (error) {
      return res.status(400).send(error.message);
    }
    return res.status(200).send({ results });
  });
};
