const retirementService = require("../services/retirement.service");
const Joi = require("joi");

// Get Countires Controller
exports.getCountries = (req, res, next) => {
  const data = {};
  retirementService.getCountries(data, (error, results) => {
    if (error) {
      console.log(error);
      return res.status(400).send(error.message);
    }
    return res.status(200).send({ message: results });
  });
};

// Get Future Expenses Controller
exports.getFutureExpenses = (req, res, next) => {
  // Validation
  const schema = Joi.object({
    age: Joi.number().integer().min(1).required(),
    expenses: Joi.number().integer().min(1).required(),
    livein: Joi.number().integer().min(1).required(),
  });

  const result = schema.validate(req.params);
  if (result.error) {
    res.status(400).send(result.error.details[0].message);
    return;
  }

  const data = {
    age: req.params.age,
    expenses: req.params.expenses,
    livein: req.params.livein,
  };
  retirementService.getFutureExpenses(data, (error, results) => {
    if (error) {
      return res.status(400).send(error.message);
    }
    return res.status(200).send({ message: results });
  });
};
